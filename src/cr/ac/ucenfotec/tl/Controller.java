package cr.ac.ucenfotec.tl;

import cr.ac.ucenfotec.bl.logic.CarreraGestor;
import cr.ac.ucenfotec.bl.logic.CursoGestor;
import cr.ac.ucenfotec.ui.UI;

public class Controller {

    private UI interfaz;
    private CarreraGestor gestorCa;
    private CursoGestor gestorCu;

    public Controller(){
        interfaz = new UI();
        gestorCa = new CarreraGestor();
        gestorCu = new CursoGestor();
    }

    public void start() throws Exception{
        int opcion= -1;
        do{
            interfaz.mostrarMenu();
            opcion = interfaz.leeerOpcion();
            procesarOpcion(opcion);
        }while (opcion!=0);
    }

    public void procesarOpcion(int pOpcion) throws Exception{
        switch (pOpcion){
            case 1:
                registrarCarrera();
                break;
            case 2:
                listarCarreras();
                break;
            case 3:
                registrarCurso();
                break;
            case 4:
                listarCursos();
                break;
            case 5:
                asociarCurso();
                break;
            case 0:
                interfaz.imprimirMensaje("Gracias por usar el programa");
                break;
            default:
                interfaz.imprimirMensaje("Opción inválida");
                break;
        }
    }

    public void registrarCarrera() throws Exception{
        interfaz.imprimirMensaje("Digite el nombre de la carrera: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el código de la carrera: ");
        String codigo = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite si la carrera está acreditada (S) o no (N): ");
        boolean acreditada = Boolean.parseBoolean(interfaz.leerTexto().equals("S")?"True":"False");

        String resultado= gestorCa.registrarCarrera(codigo,nombre,acreditada);
        interfaz.imprimirMensaje(resultado);
    }

    public void listarCarreras()throws Exception{
        gestorCa.getCarreras().forEach(carrera -> interfaz.imprimirMensaje(carrera.toString()));
    }

    public void registrarCurso()throws Exception{
        interfaz.imprimirMensaje("Digite el nombre del curso: ");
        String nombre = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite el código del curso: ");
        String codigo = interfaz.leerTexto();
        interfaz.imprimirMensaje("Digite los créditos del curso: ");
        int credito = Integer.parseInt(interfaz.leerTexto());

        String resultado= gestorCu.registrarCurso(codigo,nombre,credito);
        interfaz.imprimirMensaje(resultado);
    }

    public void listarCursos()throws Exception{
       gestorCu.getCursos().forEach(curso -> interfaz.imprimirMensaje(curso.toString()));
    }

    public void asociarCurso()throws Exception{
        listarCursos();
        interfaz.imprimirMensaje("Digite el código del curso que desea registrar: ");
        String codigoCurso= interfaz.leerTexto();
        listarCarreras();
        interfaz.imprimirMensaje("Digite el código de la carrera a la que desae agregar el curso: ");
        String codigoCarrera = interfaz.leerTexto();

        String resultado = gestorCa.asociarCurso(codigoCurso,codigoCarrera);
        interfaz.imprimirMensaje(resultado);
    }

}
